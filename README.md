This repository contains notebooks for three assignments completed during my Master's coursework in Natural Language Processing (NLP) at Stellenbosch University, as part of a course taught by Herman Kamper. The reports for each assignment are also provided.

For the first assignment, the objective was to utilize a character-level trigram language model to determine the language of a given sentence. Additionally, language model perplexity was employed to compare similarities between languages. The assignment also explored the use of byte-pair encoding to investigate language similarity.

In the second assignment, a part-of-speech tagger was built for the Sesotho language using a hidden Markov model (HMM). The HMM tagger was trained and evaluated on this specific language.

In the third assignment, the focus was on training a word embedding model, visualizing the trained embeddings, and using the averaged word embeddings for text classification on sentences.


